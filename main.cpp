#include "mainwindow.h"

#include <QApplication>
#include <QHBoxLayout>
#include <QPlainTextEdit>
//#include <QTextEdit>
#include <QWebEngineView>

#include <QFile>
#include <QTextStream>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QWidget htmlWindow;
    QHBoxLayout vbox (&htmlWindow);
    auto *plainTextEdit = new QPlainTextEdit;
    plainTextEdit->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    plainTextEdit->sizePolicy().setHorizontalStretch(1);
//    auto *textEdit = new QTextEdit;
//    textEdit->sizePolicy().horizontalStretch();
    auto *webEngineView  = new QWebEngineView ;
    webEngineView->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    webEngineView->sizePolicy().setHorizontalStretch(1);
    vbox.addWidget(plainTextEdit);
    vbox.addWidget(webEngineView);
    //vbox.addWidget(textEdit);

    webEngineView->load(QUrl("D:/QtProjects/build-untitled1-Desktop_Qt_5_15_2_MSVC2019_64bit-Debug/Data.html"));

    QObject::connect(plainTextEdit, &QPlainTextEdit::textChanged, [&webEngineView, &plainTextEdit] //, textEdit
        {
            auto text = plainTextEdit->toPlainText();

            //textEdit->setText(text);

            QString filename = "Data.html";
               QFile file(filename);
               file.remove();
               if (file.open(QIODevice::ReadWrite)) {
                   QTextStream stream(&file);
                   stream << text << endl;
               }

               webEngineView->load(QUrl("D:/QtProjects/build-untitled1-Desktop_Qt_5_15_2_MSVC2019_64bit-Debug/Data.html"));
        });

    htmlWindow.show();
    htmlWindow.setFixedSize(1500,750);

    return a.exec();
}
